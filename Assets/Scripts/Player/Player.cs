using System.Collections;
using UnityEngine;

public enum AimType
{
    Destroy, Defuse
}

[System.Serializable]
public struct AimTypeColourSet
{
    public Color LineColour;
    public Material ParticlesMaterial;
    public Material BulletMaterial;
}

public class Player : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private float MoveSpeed;
    [SerializeField] private float DashSpeed;
    [SerializeField] private float DashDampAmount;
    [SerializeField] private ParticleSystem DustParticles;

    [Header("Damage")]
    [SerializeField] private float KnockbackSpeed;
    [SerializeField] private float KnockbackDampAmount;
    [SerializeField] private float DamageColourDuration;

    [Header("Shooting")]
    [SerializeField] private Bullet BulletPrefab;
    [SerializeField] private float AimRange;
    [SerializeField, Range(0, 1)] private float AimLineFadeStart;
    [SerializeField] private LineRenderer AimLineRenderer;
    [SerializeField] private ParticleSystem AimParticles;
    [SerializeField] private AimTypeColourSet AimColourSetDestroy;
    [SerializeField] private AimTypeColourSet AimColourSetDefuse;
    [SerializeField] private float AimTypeSwitchDuration;
    [SerializeField] private Transform GunTransform;

    [Header("Appearance")]
    [SerializeField] private Transform ModelTransform;
    [SerializeField] private MeshRenderer ModelRenderer;
    
    private Rigidbody Rigidbody;
    private PlayerStats PlayerStats;

    private Vector3 Movement;
    private Vector3 DashVelocity;
    private Vector3 KnockbackVelocity;
    private float DamageColourTimer;
    private AimType CurrentAimType;
    private bool IsSwitchingAimType;

    void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();
        PlayerStats = GetComponent<PlayerStats>();
        AimLineRenderer.SetPositions(new Vector3[] {
            Vector3.zero, Vector3.forward * AimRange * AimLineFadeStart, Vector3.forward * AimRange
        });
    }

    void Start()
    {
        CameraManager.Instance.SetRaycastPlaneHeight(AimLineRenderer.transform.position.y);
    }

    void Update()
    {
        UpdateMovement();
        UpdateRotation();
        UpdateAimRaycast();
        UpdatePlayerModel();
        PollInput();
    }

    private void PollInput()
    {
        if(Input.GetMouseButtonDown(0)) Shoot();
        if(Input.GetKeyDown(KeyCode.LeftShift)) Dash();
        if(Input.GetKeyDown(KeyCode.Space)) SwitchAimType();
    }

    private void UpdateMovement()
    {
        if(!Rigidbody) return;

        Vector2 MoveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        if(MoveInput.magnitude > 1) MoveInput.Normalize();

        DashVelocity = Vector3.MoveTowards(DashVelocity, Vector3.zero, Time.deltaTime * DashDampAmount);
        KnockbackVelocity = Vector3.MoveTowards(KnockbackVelocity, Vector3.zero, Time.deltaTime * KnockbackDampAmount);

        Movement = CameraManager.Instance.transform.rotation * new Vector3(MoveInput.x, 0, MoveInput.y);
        Rigidbody.velocity = Movement * MoveSpeed + DashVelocity + KnockbackVelocity;
    }

    private void UpdateRotation()
    {
        Vector3 MousePointOnGround = CameraManager.Instance.MousePointOnGround;
        MousePointOnGround -= transform.rotation * Vector3.right * AimLineRenderer.transform.localPosition.x;
        MousePointOnGround.y = 0;
        transform.LookAt(MousePointOnGround);
    }

    private void UpdateAimRaycast()
    {
        Ray Ray = new Ray(AimLineRenderer.transform.position, transform.forward);
        float BulletRadius = BulletPrefab.GetComponent<CapsuleCollider>().radius;
        RaycastHit HitInfo;
        bool Hit = Physics.SphereCast(Ray, BulletRadius, out HitInfo, AimRange, LayerMask.GetMask("EnemyBlocks"));
        
        if(Hit)
        {
            Box EnemyBlockHit = HitInfo.collider.GetComponent<Box>();
            if(EnemyBlockHit)
            {
                AimParticles.Play();
                AimParticles.transform.position = HitInfo.point;
                AimParticles.transform.rotation = Quaternion.LookRotation(HitInfo.normal, Vector3.up);
                EnemyBlockHit.AimLineHit(CurrentAimType);
            }
        }
        else
        {
            AimParticles.Stop();
        }
    }

    private void UpdatePlayerModel()
    {
        float ModelScaleY = Mathf.Lerp(ModelTransform.localScale.y, 1 - 0.5f * (DashVelocity.magnitude / DashSpeed), Time.deltaTime * 15);
        ModelTransform.localScale = new Vector3(1 + (1 - ModelScaleY), ModelScaleY, 1 + (1 - ModelScaleY));

        DamageColourTimer = Mathf.MoveTowards(DamageColourTimer, 0, Time.deltaTime);
        ModelRenderer.sharedMaterial.SetColor("_EmissionColor", DamageColourTimer > 0 ? Color.red * 0.5f : Color.black);

        float GunTargetAngle = IsSwitchingAimType ? 50.0f : 0.0f;
        float GunAngle = Mathf.LerpAngle(GunTransform.localRotation.eulerAngles.x, GunTargetAngle, Time.deltaTime * 20);
        GunTransform.localRotation = Quaternion.Euler(GunAngle, 0, 0);
    }

    private void Shoot()
    {
        if(IsSwitchingAimType) return;

        float EnergyCost = 20.0f;
        if(PlayerStats.CurrentEnergy >= EnergyCost)
        {
            Bullet SpawnedBullet = Instantiate(BulletPrefab, AimLineRenderer.transform.position, AimLineRenderer.transform.rotation);
            AimTypeColourSet ColourSet = CurrentAimType == AimType.Destroy ? AimColourSetDestroy : AimColourSetDefuse;
            SpawnedBullet.Fire(AimLineRenderer.transform.forward, AimRange, CurrentAimType, ColourSet.BulletMaterial);
            PlayerStats.UseEnergy(EnergyCost);
            AudioManager.Instance.SfxBullet.PlayAsSFX(Random.Range(0.6f, 1.0f));
        }
        else
        {
            MenuManager.Instance.Hud.EnergyBar.Shake(3, 0.01f, 0.05f);
            AudioManager.Instance.SfxNoEnergy.PlayAsSFX();
        }
    }

    private void SwitchAimType()
    {
        if(!IsSwitchingAimType)
        {
            StartCoroutine(SwitchAimTypeIEnum());
        }
    }

    private IEnumerator SwitchAimTypeIEnum()
    {
        CurrentAimType = CurrentAimType == AimType.Destroy ? AimType.Defuse : AimType.Destroy;
        AimTypeColourSet ColourSet = CurrentAimType == AimType.Destroy ? AimColourSetDestroy : AimColourSetDefuse;
        AimLineRenderer.startColor = new Color(ColourSet.LineColour.r, ColourSet.LineColour.g, ColourSet.LineColour.b, AimLineRenderer.startColor.a);
        AimLineRenderer.endColor = new Color(ColourSet.LineColour.r, ColourSet.LineColour.g, ColourSet.LineColour.b, AimLineRenderer.endColor.a);
        AimParticles.GetComponent<ParticleSystemRenderer>().material = ColourSet.ParticlesMaterial;

        AimParticles.gameObject.SetActive(false);
        AimLineRenderer.gameObject.SetActive(false);

        IsSwitchingAimType = true;
        MenuManager.Instance.Hud.HideEnergyBar();
        AudioManager.Instance.SfxSwitchAimType.PlayAsSFX(0.5f);

        yield return new WaitForSeconds(AimTypeSwitchDuration);

        AimParticles.gameObject.SetActive(true);
        AimLineRenderer.gameObject.SetActive(true);

        MenuManager.Instance.Hud.SetEnergyBarColour(CurrentAimType);
        MenuManager.Instance.Hud.ShowEnergyBar();
        AudioManager.Instance.SfxSwitchAimType.PlayAsSFX(0.75f);

        IsSwitchingAimType = false;
    }

    private void Dash()
    {
        if(Movement.magnitude == 0) return;

        float EnergyCost = 40.0f;
        if(PlayerStats.CurrentEnergy >= EnergyCost)
        {
            DashVelocity = Movement.normalized * DashSpeed;
            PlayerStats.UseEnergy(EnergyCost);
            DustParticles.Play();
            AudioManager.Instance.SfxDash.PlayAsSFX(Random.Range(0.6f, 1.0f));
        }
        else
        {
            MenuManager.Instance.Hud.EnergyBar.Shake(3, 0.01f, 0.05f);
            AudioManager.Instance.SfxNoEnergy.PlayAsSFX();
        }
    }

    public void Damage(Vector3 KnockbackDirection, int Health)
    {
        DamageColourTimer = DamageColourDuration;
        ModelRenderer.sharedMaterial.EnableKeyword("_EMISSION");
        CameraManager.Instance.ShakeCamera(0.5f, 0.01f, 0.1f);
        AudioManager.Instance.SfxPlayerHit.PlayAsSFX(Random.Range(0.6f, 1.0f));
        Knockback(KnockbackDirection, KnockbackSpeed);
        PlayerStats.Damage(Health);
    }

    public void Knockback(Vector3 Direction, float Speed)
    {
        KnockbackVelocity = Direction.normalized * Speed;
        DashVelocity = Vector3.zero;
    }

    void OnCollisionEnter(Collision other) 
    {
        Enemy Enemy = other.gameObject.GetComponent<Enemy>();
        if(Enemy)
        {
            Vector3 Direction = (transform.position - other.transform.position).normalized;
            Damage(Direction, 5);
            Enemy.Knockback(-Direction);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    [SerializeField] private float MaxHealth, MaxEnergy;
    [SerializeField] private float EnergyRechargeRate;

    public float CurrentHealth { get; private set; }
    public float CurrentEnergy { get; private set; }

    void Awake() {
        CurrentHealth = MaxHealth;
        CurrentEnergy = MaxEnergy;
    }

    void Update() 
    {
        // Recharge energy
        UseEnergy(-Time.deltaTime * EnergyRechargeRate);
    }

    public void Damage(float Value)
    {
        CurrentHealth = Mathf.Clamp(CurrentHealth - Value, 0, MaxHealth);
        if(MenuManager.Instance.Hud && MenuManager.Instance.Hud.HealthBar.gameObject.activeInHierarchy)
        {
            MenuManager.Instance.Hud.HealthBar.SetValues(MaxHealth, CurrentHealth);
            MenuManager.Instance.Hud.HealthBar.Shake(5, 0.01f, 0.05f);
        }

        if(CurrentHealth == 0)
        {
            GameService.Instance.EndGame();
        }
    }

    public void UseEnergy(float Value)
    {   
        CurrentEnergy = Mathf.Clamp(CurrentEnergy - Value, 0, MaxEnergy);
        if(MenuManager.Instance.Hud && MenuManager.Instance.Hud.EnergyBar)
        {
            MenuManager.Instance.Hud.EnergyBar.SetValues(MaxEnergy, CurrentEnergy);
        }
    }
}

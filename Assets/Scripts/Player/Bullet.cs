using UnityEngine;

public class Bullet : MonoBehaviour 
{
    [SerializeField] private float Speed;
    [SerializeField] private Rigidbody Rigidbody;
    [SerializeField] private LineRenderer Renderer;

    private Vector3 Origin;
    private float Range;
    private AimType AimType;
    private bool HasHit;

    void Update() 
    {
        if(Vector3.Distance(transform.position, Origin) > Range)
        {
            Destroy(gameObject);
        }
    }

    public void Fire(Vector3 Direction, float Range, AimType AimType, Material Material)
    {
        Rigidbody.velocity = Direction.normalized * Speed;
        Renderer.material = Material;
        Origin = transform.position;
        this.Range = Range;
        this.AimType = AimType;
    }

    private void OnTriggerEnter(Collider other) 
    {
        Box Box = other.GetComponent<Box>();
        if(Box && !HasHit)
        {
            Destroy(gameObject);
            HasHit = true;
            Box.OnHit(AimType, transform.forward);
        }
    }
}
using System;
using UnityEngine;

public class BoxFace
{
    public Box Owner;
    public Vector3 Direction;
}

public class Box : MonoBehaviour
{
    [SerializeField] private ParticleSystem ExplosionParticlesPrefab;
    [SerializeField] private ParticleSystem SparksParticlesPrefab;
    [SerializeField] private ParticleSystem SmokeParticlesPrefab;
    [SerializeField] private GameObject BombIcon;
    [SerializeField] private bool ShowBombIcon;
    [SerializeField] private MeshRenderer Renderer;
    [SerializeField] private Color AimedAtDestroyColour;
    [SerializeField] private Color AimedAtDefuseColour;
    [SerializeField] private Color DeactivatedColour;

    public int RowIndex { get; private set; }
    public int ColumnIndex { get; private set; }
    public int DangerNearby { get; private set; }
    public bool IsDangerous { get; private set; }
    public bool IsDeactivated { get; private set; }

    private Material DefaultMaterial;
    private Material AimedAtDestroyMaterial;
    private Material AimedAtDefuseMaterial;
    private Material DeactivatedMaterial;
    private bool IsAimedAt;
    private AimType AimedAtType;

    private Action<Box, AimType, Vector3> _changeCallback;
    private Action<Box> _aimLineHitCallback;

    public static float BoxSize = 1.0f;

    void Awake() {
        DefaultMaterial = Renderer.sharedMaterial;

        AimedAtDestroyMaterial = new Material(DefaultMaterial);
        AimedAtDestroyMaterial.color = AimedAtDestroyColour;

        AimedAtDefuseMaterial = new Material(DefaultMaterial);
        AimedAtDefuseMaterial.color = AimedAtDefuseColour;

        DeactivatedMaterial = new Material(DefaultMaterial);
        DeactivatedMaterial.color = DeactivatedColour;
    }

    void Update() 
    {
        if(IsAimedAt)
        {
            Renderer.material = AimedAtType == AimType.Destroy ? 
                AimedAtDestroyMaterial : AimedAtDefuseMaterial;
            IsAimedAt = false;
        }
        else
        {
            Renderer.material = IsDeactivated ? DeactivatedMaterial : DefaultMaterial;
        }

        Renderer.transform.localScale = Vector3.MoveTowards
        (
            Renderer.transform.localScale,
            Vector3.one * (IsDeactivated ? 0.9f : 1.0f),
            Time.deltaTime * 5
        );

        float Angle = Mathf.Lerp(Renderer.transform.localRotation.eulerAngles.y, 
            IsDeactivated ? 359.99f : 0.0f, Time.deltaTime * 15);
        Renderer.transform.localRotation = Quaternion.Euler(Vector3.up * Angle);
    }

    // Initialise values
    public void Setup(int Column, int Row)
    {
        ColumnIndex = Column;
        RowIndex = Row;
    }

    // Set bomb, adjancent bomb number and onclick callback values
    public void Charge(int dangerNearby, bool danger, Action<Box, AimType, Vector3> onChange, Action<Box> onAimLineHit)
    {
        _changeCallback = onChange;
        _aimLineHitCallback = onAimLineHit;
        DangerNearby = dangerNearby;
        IsDangerous = danger;
        BombIcon.SetActive(IsDangerous && ShowBombIcon);
    }

    public void OnHit(AimType AimType, Vector3 HitDir)
    {
        _changeCallback?.Invoke(this, AimType, HitDir);
    }

    public void AimLineHit(AimType AimType)
    {
        IsAimedAt = true;
        AimedAtType = AimType;
        _aimLineHitCallback?.Invoke(this);
    }

    public void ToggleDeactivated()
    {
        IsDeactivated = !IsDeactivated;

        AudioManager.Instance.SfxDefuse.PlayAsSFX(IsDeactivated ? 0.6f : 0.55f);
    }

    public void Explode(bool Smoke)
    {
        if(Smoke)
        {
            Instantiate(SmokeParticlesPrefab, transform.position, Quaternion.identity);
        }
        else
        {
            Instantiate(ExplosionParticlesPrefab, transform.position, Quaternion.identity);
            Instantiate(SparksParticlesPrefab, transform.position, Quaternion.identity);
            AudioManager.Instance.SfxExplosion.PlayAsSFX(UnityEngine.Random.Range(0.6f, 1.0f));
        }
        
        Destroy(gameObject);
    }
}

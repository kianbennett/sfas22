using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public static class WebRequestUtil
{
    private static string DreamloURL = "http://dreamlo.com/lb/KmmDXf7twUyyVgZPdBvaug_vOaYTVjxEK04XtQE4dxVg";

    public static IEnumerator UploadHighScoreIEnum(Highscore Highscore, UnityAction<string> OnComplete, UnityAction OnError) 
    {
        UnityWebRequest Request = UnityWebRequest.Get(DreamloURL + "/add-pipe/" + UnityWebRequest.EscapeURL(Highscore.Name) + "/" + Highscore.Score);
        Request.timeout = 5;
        Debug.Log("Sending request to " + Request.url);
		yield return Request.SendWebRequest();

        if(Request.result == UnityWebRequest.Result.ConnectionError) 
        {
            Debug.LogError(Request.error);
            OnError();
        } 
        else 
        {
            Debug.Log("Upload successful");
            string Results = Request.downloadHandler.text;
            OnComplete(Results);
        }
    }

    public static IEnumerator GetHighScoresIEnum(int Count, UnityAction<string> OnComplete, UnityAction OnError) 
    {
        UnityWebRequest Request = UnityWebRequest.Get(DreamloURL + "/pipe/" + Count);
        Request.timeout = 5;
        Debug.Log("Sending request to " + Request.url);
        yield return Request.SendWebRequest();

        if (Request.result == UnityWebRequest.Result.ConnectionError) 
        {
            Debug.LogError(Request.error);
            OnError();
        } 
        else 
        {
            Debug.Log("Request successful");
            string Results = Request.downloadHandler.text;
            OnComplete(Results);
        }
    }

    public static Highscore[] GetHighscoresFromString(string Result)
    {
        List<Highscore> Highscores = new List<Highscore>();
        string[] Lines = Result.Split('\n');

        for(int i = 0; i < Lines.Length; i++)
        {
            string Line = Lines[i];
            if(Line == "") continue;
            string[] PipeSplit = Line.Split('|');
            if(PipeSplit.Length >= 2)
            {
                Highscore Highscore = new Highscore();
                Highscore.Name = PipeSplit[0];
                Highscore.Score = int.Parse(PipeSplit[1]);
                Highscores.Add(Highscore);
            }
        }
        return Highscores.ToArray();
    }
}
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Board : MonoBehaviour
{
    public enum Event { ClickedBlank, ClickedNearDanger, ClickedDanger, Win };

    [SerializeField] private Transform BoxContainer;
    [SerializeField] private Transform BombIndicatorContainer;
    [SerializeField] private Box BoxPrefab;
    [SerializeField] private BombIndicatorBlock BombIndicatorBlockPrefab;
    [SerializeField] private bool ShowBombIndicators;

    private Enemy Owner;
    private int Width;
    private int Height;
    private Box[,] _grid;
    private bool[,] _bombLocations;
    private RectTransform _rect;
    private Action<Event> _clickEvent;
    private Dictionary<Box, List<BombIndicatorBlock>> BombIndicatorDictionary;

    public List<BoxFace> OutwardBoxFaces { get; private set; }
    public int BombCount { get; private set; }

    private static Vector2Int[] _neighbours = new Vector2Int[8]
    {
        new Vector2Int(-1, -1), // Bottom left
        new Vector2Int(0, -1),  // Bottom
        new Vector2Int(1, -1),  // Bottom Right
        new Vector2Int(-1, 0),  // Left
        new Vector2Int(1, 0),   // Right
        new Vector2Int(-1, 1),  // Top Left
        new Vector2Int(0, 1 ),  // Top
        new Vector2Int(1, 1)    // Top Right
    };
    private static Vector2Int[] _adjacentNeighbours = new Vector2Int[4]
    {
        new Vector2Int(0, -1),  // Bottom
        new Vector2Int(-1, 0),  // Left
        new Vector2Int(1, 0),   // Right
        new Vector2Int(0, 1 ),  // Top
    };

    // Generate cells
    public void RechargeBoxes(int BombAreaWidth, int BombAreaHeight, int BombCount)
    { 
        this.BombCount = BombCount;

        // Randomly place N bombs in an X by Y grid
        // Create grid one size bigger on each side than the bomb grid
        int numberOfItems = BombAreaWidth * BombAreaHeight;
        List<bool> dangerList = new List<bool>(numberOfItems);

        for (int count = 0; count < numberOfItems; ++count)
        {
            dangerList.Add(count < BombCount);
        }

        dangerList.RandomShuffle();

        // Board size is one bigger than bomb area extents on each side
        Width = BombAreaWidth + 2;
        Height = BombAreaHeight + 2;

        // Set bomb locations in the new sized board array
        _bombLocations = new bool[Width, Height];

        for(int Column = 0; Column < BombAreaWidth; ++Column)
        {
            for(int Row = 0; Row < BombAreaHeight; ++Row)
            {
                int Index = Row * BombAreaWidth + Column;
                _bombLocations[Column + 1, Row + 1] = dangerList[Index];
            }
        }

        _grid = new Box[Width, Height];

        Clear();
        Build(_bombLocations);
        PlaceBombIndicators();
        CalculateOutwardBoxFaces();
        CalculateAllDangerNearby(_bombLocations);
    }

    public void Build(bool[,] BombLocations)
    {
        // For each cell:
        //      If a cell has a bomb, add a box
        //      If a cell has no bomb, and all its neighbours are nearby at least 1 bomb, then add a box
        //      If a cell has no bomb or box and is nearby a bomb, add a bomb indicator

        for(int Column = 0; Column < Width; ++Column)
        {
            for(int Row = 0; Row < Height; ++Row)
            {
                bool AllNeighboursNearbyABomb = true;
                for (int count = 0; count < _neighbours.Length; ++count)
                {
                    int NeighbourColumn = Column + _neighbours[count].x;
                    int NeighbourRow = Row + _neighbours[count].y;

                    bool IsValid = IsValidCoordinate(NeighbourColumn, NeighbourRow);

                    if(!IsValid)
                    {
                        AllNeighboursNearbyABomb = false;
                        break;
                    }

                    bool IsBomb = BombLocations[NeighbourColumn, NeighbourRow];
                    bool NoNearbyBombs = CountDangerNearby(BombLocations, _neighbours, NeighbourColumn, NeighbourRow) == 0;

                    if(!IsBomb && NoNearbyBombs)
                    {
                        AllNeighboursNearbyABomb = false;
                        break;    
                    }
                }

                bool AllAdjacentAreBombs = CountDangerNearby(BombLocations, _adjacentNeighbours, Column, Row) == _adjacentNeighbours.Length;

                if(!AllAdjacentAreBombs)
                {
                    if(BombLocations[Column, Row] || AllNeighboursNearbyABomb)
                    {
                        PlaceBox(Column, Row);
                    }    
                }
            }
        }

        RemoveInvalidBoxesFromGrid();
    }

    public void PlaceBombIndicators()
    {
        ClearBombIndicators();
        
        for(int Column = 0; Column < Width; ++Column)
        {
            for(int Row = 0; Row < Height; ++Row)
            {
                if(_grid[Column, Row] == null)
                {
                    int NearbyBombs = CountDangerNearby(_bombLocations, _neighbours, Column, Row);
                    if(NearbyBombs > 0 && ShowBombIndicators)
                    {
                        PlaceBombIndicator(Column, Row, _bombLocations);
                    }
                } 
            }
        }
    }

    public void ClearBombIndicators()
    {
        for(int i = BombIndicatorContainer.childCount - 1; i >= 0; --i)
        {
            Destroy(BombIndicatorContainer.GetChild(i).gameObject);
        }

        if(BombIndicatorDictionary != null)
        {
            BombIndicatorDictionary.Clear();
        } else
        {
            BombIndicatorDictionary = new Dictionary<Box, List<BombIndicatorBlock>>();
        }
    }

    public void SetOwner(Enemy Owner)
    {
        this.Owner = Owner;
    }

    public void PlaceBox(int Column, int Row)
    {
       Vector3 BlockPosition = new Vector3(
            Column * Box.BoxSize - Width * Box.BoxSize / 2.0f,
            0,
            Row * Box.BoxSize - Height * Box.BoxSize / 2.0f
        );

        Box NewBox = Instantiate(BoxPrefab, BlockPosition, Quaternion.identity);
        NewBox.Setup(Column, Row);
        NewBox.transform.SetParent(BoxContainer, false);
        _grid[Column, Row] = NewBox;
    }

    public void PlaceBombIndicator(int Column, int Row, bool[,] BombLocations)
    {
        Vector3 BlockPosition = new Vector3(
            -Width * Box.BoxSize / 2.0f + Column * Box.BoxSize,
            0,
            -Height * Box.BoxSize / 2.0f + Row * Box.BoxSize
        );

        BombIndicatorBlock BombIndicator = Instantiate(BombIndicatorBlockPrefab, BlockPosition, Quaternion.identity);
        BombIndicator.transform.SetParent(BombIndicatorContainer, false);
        BombIndicator.SetNearbyBombCount(CountDangerNearby(BombLocations, _neighbours, Column, Row));

        foreach(Box DangerBox in GetNearbyBoxes(BombLocations, _neighbours, Column, Row))
        {
            if(!DangerBox) continue;
            if(BombIndicatorDictionary.ContainsKey(DangerBox))
            {
                BombIndicatorDictionary[DangerBox].Add(BombIndicator);
            }
            else
            {
                BombIndicatorDictionary.Add(DangerBox, new List<BombIndicatorBlock>() { BombIndicator });
            }
        }
    }

    public void Setup(Action<Event> onClickEvent)
    {
        _clickEvent = onClickEvent;
        Clear();
    }

    // Reset all cells in the grid
    public void Clear()
    {
        for(int i = BoxContainer.childCount - 1; i >= 0; --i)
        {
            Destroy(BoxContainer.GetChild(i).gameObject);
        }
        for(int i = BombIndicatorContainer.childCount - 1; i >= 0; --i)
        {
            Destroy(BombIndicatorContainer.GetChild(i).gameObject);
        }
    }

    // Count how many bombs are adjacent to a given cell (if there is a bomb in the cell return 0)
    private int CountDangerNearby(bool[,] BombLocations, Vector2Int[] Neighbours, int Column, int Row)
    {
        int result = 0;

        if (IsValidCoordinate(Column, Row) && !BombLocations[Column, Row])
        {
            for (int count = 0; count < Neighbours.Length; ++count)
            {
                int NeighbourColumn = Column + Neighbours[count].x;
                int NeighbourRow = Row + Neighbours[count].y;

                if(IsValidCoordinate(NeighbourColumn, NeighbourRow))
                {
                    result += BombLocations[NeighbourColumn, NeighbourRow] ? 1 : 0;
                }
            }
        }

        return result;
    }

    public Box[] GetNearbyBoxes(bool[,] BombLocations, Vector2Int[] Neighbours, int Column, int Row)
    {
        List<Box> Boxes = new List<Box>();

        if(IsValidCoordinate(Column, Row) && !BombLocations[Column, Row])
        {
            for (int count = 0; count < Neighbours.Length; ++count)
            {
                int NeighbourColumn = Column + Neighbours[count].x;
                int NeighbourRow = Row + Neighbours[count].y;

                if(IsValidCoordinate(NeighbourColumn, NeighbourRow))
                {
                    if(_grid[NeighbourColumn, NeighbourRow])
                    {
                        Boxes.Add(_grid[NeighbourColumn, NeighbourRow]);
                    }
                }
            }
        }

        return Boxes.ToArray();
    }

    public void CalculateOutwardBoxFaces()
    {
        OutwardBoxFaces = new List<BoxFace>();

        for(int Column = 0; Column < Width; ++Column)
        {
            for(int Row = 0; Row < Height; ++Row)
            {
                Box Box = _grid[Column, Row];
                
                if(_grid[Column, Row])
                {
                    for (int Count = 0; Count < _adjacentNeighbours.Length; ++Count)
                    {
                        int NeighbourColumn = Column + _adjacentNeighbours[Count].x;
                        int NeighbourRow = Row + _adjacentNeighbours[Count].y;

                        if(!IsValidCoordinate(NeighbourColumn, NeighbourRow) || _grid[NeighbourColumn, NeighbourRow] == null)
                        {
                            BoxFace Face = new BoxFace
                            {
                                Owner = Box,
                                Direction = new Vector3(_adjacentNeighbours[Count].x, 0, _adjacentNeighbours[Count].y)
                            };

                            OutwardBoxFaces.Add(Face);
                        }
                    }
                }
            }
        }
    }

    private void CalculateAllDangerNearby(bool[,] BombLocations)
    {
        // Update boxes to show/hide, nearby bomb count etc
        for (int Row = 0; Row < Height; ++Row)
        {
            for (int Column = 0; Column < Width; ++Column)
            {   
                if(_grid[Column, Row])
                {
                    _grid[Column, Row].Charge(
                        CountDangerNearby(BombLocations, _neighbours, Column, Row), 
                        BombLocations[Column, Row], 
                        OnHitBox,
                        OnAimLineHitBox
                    );
                }
            }
        }
    }

    // Call an event when a cell is clicked on
    private void OnHitBox(Box Box, AimType AimType, Vector3 HitDir)
    {
        if(Owner && Owner.IsDeactivated) return;

        if(AimType == AimType.Destroy)
        {
            RemoveBoxFromGrid(Box);

            CalculateOutwardBoxFaces();
            CalculateAllDangerNearby(_bombLocations);
            PlaceBombIndicators();

            if(!Box.IsDangerous)
            {
                GameService.Instance.IncreaseScore(5);
            }

            Box.Explode(false);
            Owner.OnBoxDestroy(Box, true);
            Owner.Knockback(HitDir);
        }
        else
        {
            Box.ToggleDeactivated();
        }

        if(CheckForWin())
        {
            Owner.Deactivate();
        }
    }

    // Return true if there are no more active boxes that don't have a bomb in them
    // and all bomb boxes are deactivated
    private bool CheckForWin()
    {
        bool Result = true;

        for(int Column = 0; Column < Width; Column++)
        {
            for(int Row = 0; Row < Height; Row++)
            {
                Box Box = _grid[Column, Row];
                if(Box)
                {
                    if(!Box.IsDangerous || !Box.IsDeactivated)
                    {
                        Result = false;
                    }
                }
            }
        }

        return Result;
    }

    private void ClearNearbyBlanks(Box box)
    {
        RecursiveClearBlanks(box);
    }

    // Reveal the cell if it doesn't have a bomb in it, then recursively reveal all its neighbours (and their neighbours, etc) if they have no bombs
    private void RecursiveClearBlanks(Box box)
    {
        if (!box.IsDangerous && box.DangerNearby == 0)
        {
            for (int count = 0; count < _neighbours.Length; ++count)
            {
                int NeighbourColumn = box.ColumnIndex + _neighbours[count].x;
                int NeighbourRow = box.RowIndex + _neighbours[count].y;
                bool Active = IsValidCoordinate(NeighbourColumn, NeighbourRow) && _grid[NeighbourColumn, NeighbourRow];

                if (Active)
                {
                    RecursiveClearBlanks(_grid[NeighbourColumn, NeighbourRow]);
                }
            }
        }
    }

    // Returns true if the coordinate doesn't lie outside the bounds of the array
    private bool IsValidCoordinate(int Column, int Row, bool LogWarning = false)
    {
        bool ValidCoordinate = Column >= 0 && Column < Width && Row >= 0 && Row < Height;

        if(LogWarning && !ValidCoordinate)
        {
            Debug.LogWarningFormat("Coordinate [{0}, {1}] is invalid", Column, Row);
        }

        return ValidCoordinate;
    }

    private void RemoveInvalidBoxesFromGrid()
    {
        for(int Column = 0; Column < Width; ++Column)
        {
            for(int Row = 0; Row < Height; ++Row)
            {
                if(_grid[Column, Row])
                {
                    // If a tile surrounded by all bombs, remove it
                    bool AllNeighboursAreBombs = true;
                    for (int count = 0; count < _adjacentNeighbours.Length; ++count)
                    {
                        int NeighbourColumn = Column + _adjacentNeighbours[count].x;
                        int NeighbourRow = Row + _adjacentNeighbours[count].y;

                        if(IsValidCoordinate(NeighbourColumn, NeighbourRow))
                        {
                            if(!_bombLocations[NeighbourColumn, NeighbourRow])
                            {
                                AllNeighboursAreBombs = false;
                            }
                        }
                    }

                    if(AllNeighboursAreBombs)
                    {
                    }
                }
            }
        }
    }

    public Box[] GetAllBoxes()
    {
        List<Box> Boxes = new List<Box>();

        for(int Column = 0; Column < Width; Column++)
        {
            for(int Row = 0; Row < Height; Row++)
            {
                Box Box = _grid[Column, Row];
                if(Box) Boxes.Add(Box);
            }
        }

        return Boxes.ToArray();
    }

    public void OnAimLineHitBox(Box Box)
    {
        if(BombIndicatorDictionary.ContainsKey(Box) && BombIndicatorDictionary[Box] != null)
        {
            foreach(BombIndicatorBlock BombIndicator in BombIndicatorDictionary[Box])
            {
                BombIndicator.SetHighlighted();
            }
        }
        
    }

    public void RemoveBoxFromGrid(Box Box)
    {
        if(IsValidCoordinate(Box.ColumnIndex, Box.RowIndex))
        {
            _grid[Box.ColumnIndex, Box.RowIndex] = null;
        }
    }
}

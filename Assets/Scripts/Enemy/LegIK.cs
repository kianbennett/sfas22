using UnityEngine;

[ExecuteInEditMode]
public class LegIK : MonoBehaviour
{
    [Header("Joints")]
    [SerializeField] private Transform JointUpper;
    [SerializeField] private Transform JointLower;
    [SerializeField] private Transform Foot;

    [Header("Bones")]
    [SerializeField] private Transform BoneUpper;
    [SerializeField] private Transform BoneLower;
    [SerializeField] private float BoneLengthUpper = 1;
    [SerializeField] private float BoneLengthLower = 1;
    [SerializeField] private float BoneWidth = 0.15f;

    [Header("Target")]
    [SerializeField] private Transform Target;

    void Awake()
    {
        BoneUpper.localScale = new Vector3(BoneLengthUpper, BoneWidth, BoneWidth);
        BoneLower.localScale = new Vector3(BoneLengthLower, BoneWidth, BoneWidth);
        BoneUpper.transform.localPosition = Vector3.right * BoneLengthUpper * 0.5f;
        JointLower.transform.localPosition = Vector3.right * BoneLengthUpper;
        BoneLower.transform.localPosition = Vector3.right * BoneLengthLower * 0.5f;
        Foot.transform.localPosition = Vector3.right * BoneLengthLower;
    }

    void Update()
    {
        UpdateJoints();
    }

    private void UpdateJoints()
    {
        float JointUpperAngle = 0;
        float JointLowerAngle = 0;

        // Contruct triangle ABC where A is JointUpper, B is JointLower, C is the Target
        // BC = a, AC = b, AB = c
        // https://www.alanzucconi.com/2018/05/02/ik-2d-1/

        // Angle of the arm on the Y axis, relative to XY plane
        // tan(ϴ) = 𐤃z / 𐤃x
        float Theta = Mathf.Atan2(Target.position.z - JointUpper.position.z, Target.position.x - JointUpper.position.x) * Mathf.Rad2Deg;

        // Angle from AC to X axis
        // tan(A2) = (Cy - Ay) / (Cx - Ax)
        Vector3 VectorAC = Target.position - JointUpper.position;
        // Has to be rotated around the Y axis to put it back on the XY plane
        VectorAC = Quaternion.Euler(Vector3.up * Theta) * VectorAC;
        // Reverse sign of X to make the arm angle in the opposite direction
        float AngleA2 = Mathf.Atan2(VectorAC.y, -VectorAC.x) * Mathf.Rad2Deg;

        // Length of AC
        float DistTargetToJointUpper = Vector3.Distance(JointUpper.position, Target.position);

        // If the leg isn't fully stretched
        if(DistTargetToJointUpper < BoneLengthUpper + BoneLengthLower)
        {
            // Inner angle between AB and AC
            // cos(AngleA) = (b² + c² - a²) / 2bc
            float CosAngleA = (Mathf.Pow(DistTargetToJointUpper, 2) + Mathf.Pow(BoneLengthUpper, 2) - 
                Mathf.Pow(BoneLengthLower, 2)) / (2 * DistTargetToJointUpper * BoneLengthUpper);
            float AngleA = Mathf.Acos(CosAngleA) * Mathf.Rad2Deg;

            // Inner angle between BA and BC
            // cos(AngleB) = (a² + c² - b²) / 2ac
            float CosAngleB = (Mathf.Pow(BoneLengthLower, 2) + Mathf.Pow(BoneLengthUpper, 2) - 
                Mathf.Pow(DistTargetToJointUpper, 2)) / (2 * BoneLengthLower * BoneLengthUpper);
            float AngleB = Mathf.Acos(CosAngleB) * Mathf.Rad2Deg;

            JointUpperAngle = AngleA2 - AngleA;
            JointLowerAngle = 180.0f - AngleB;    
        }
        else
        {
            JointUpperAngle = AngleA2;
            JointLowerAngle = 0.0f;
        }

        Vector3 JointUpperEulers = JointUpper.eulerAngles;
        JointUpperEulers.y = 180.0f - Theta;
        JointUpperEulers.z = JointUpperAngle;
        JointUpper.eulerAngles = JointUpperEulers;

        Vector3 JointLowerEulers = JointLower.localEulerAngles;
        JointLowerEulers.z = JointLowerAngle;
        JointLower.localEulerAngles = JointLowerEulers;
    }

    public Vector3 GetTargetPosition()
    {
        return Target.position;
    }

    public void SetTargetPosition(Vector3 Pos)
    {
        Target.position = Pos;
    }
}

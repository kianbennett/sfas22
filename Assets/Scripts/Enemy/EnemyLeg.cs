using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct LegParams
{
    public float DistUntilStep;
    public float Overstep;
    public float StepSpeed;
    public float StepHeight;
}

[ExecuteInEditMode]
public class EnemyLeg : MonoBehaviour
{
    [SerializeField] private LegIK LegIK;
    [SerializeField] private Transform LegTarget;
    [SerializeField, Range(0, 1)] private float Offset;

    private Enemy Owner;
    private LegParams Params;
    private Vector3 LegPosition;
    private Vector3 NewLegPosition;
    private Vector3 OldLegPosition;

    void Awake()
    {
        if(!Owner)
        {
            Setup(GetComponentInParent<Enemy>(), Params);
        }
    }

    void Update()
    {
        LegIK.SetTargetPosition(LegPosition);
        LegTarget.position = new Vector3(LegTarget.position.x, 0, LegTarget.position.z);

        float DistFromTarget = Vector3.Distance(LegIK.GetTargetPosition(), LegTarget.position);
        Debug.DrawLine(LegIK.GetTargetPosition(), LegTarget.position, Color.red, Time.deltaTime);

        if(DistFromTarget > Params.DistUntilStep)
        {
            Vector3 Direction = (LegTarget.position - LegIK.GetTargetPosition()).normalized;
            if(Owner)
            {
                Direction = Quaternion.Euler(Owner.GetAngularVelocity()) * Owner.GetVelocity();
            }
            NewLegPosition = LegTarget.position + Direction.normalized * Params.Overstep;
            OldLegPosition = LegPosition;
        }

        float ActualStepSpeed = Params.StepSpeed;
        if(Owner)
        {
            ActualStepSpeed += Mathf.Max(Owner.GetVelocity().magnitude, Owner.GetAngularVelocity().magnitude * 2);
        }
        LegPosition = Vector3.MoveTowards(LegPosition, NewLegPosition, Time.deltaTime * ActualStepSpeed);
        // LegPosition = Vector3.Lerp(LegPosition, NewLegPosition, Time.deltaTime * ActualStepSpeed);

        // Raise leg when moving
        Vector3 LegMovementTotal = NewLegPosition - OldLegPosition;
        Vector3 LegMovementCurrent = LegPosition - OldLegPosition;
        float LegMovementPercentage = LegMovementTotal.magnitude > 0 ? 
            LegMovementCurrent.magnitude / LegMovementTotal.magnitude : 0;
        LegPosition.y = Params.StepHeight * Mathf.Sin(LegMovementPercentage * Mathf.PI);
    }

    public void Setup(Enemy Owner, LegParams Params, float Offset = 0)
    {
        this.Owner = Owner;
        this.Params = Params;
        this.Offset = Mathf.Clamp01(Offset);
        Reset();
    }

    public void Reset()
    {
        if(Owner)
        {
            LegPosition = LegTarget.position + Owner.transform.forward * Params.DistUntilStep * Offset;
        } 
        else 
        {
            LegPosition = LegTarget.position;
        }
        
        LegPosition.y = 0;
        NewLegPosition = LegPosition;
        OldLegPosition = LegPosition;
    }

    public float GetCurrentStepHeight()
    {
        return LegPosition.y;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0.4f, 0.4f, 0.7f);
        Gizmos.DrawSphere(LegTarget.position, 0.15f);
    }
}
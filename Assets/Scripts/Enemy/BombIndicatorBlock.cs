using System.Collections.Generic;
using UnityEngine;
using TMPro;

[System.Serializable]
public struct BombIndicatorColourSet
{
    public Color LineColour;
    public Color FillColour;
    public Material NumberTextMaterial;
}

public class BombIndicatorBlock : MonoBehaviour
{
    [SerializeField] private LineRenderer[] TopFaceLines;
    [SerializeField] private LineRenderer[] BottomFaceLines;
    [SerializeField] private LineRenderer[] LeftFaceLines;
    [SerializeField] private LineRenderer[] RightFaceLines;
    [SerializeField] private LineRenderer[] FrontFaceLines;
    [SerializeField] private LineRenderer[] BackFaceLines;

    [SerializeField] private MeshRenderer FillRenderer;
    [SerializeField] private TextMeshPro NumberText;
    // [SerializeField] private Color[] DangerColors = new Color[8];
    [SerializeField] private BombIndicatorColourSet[] DangerColorSets = new BombIndicatorColourSet[8];

    private Dictionary<Vector3, LineRenderer[]> FaceLines;
    private bool IsHighlighted;

    void Awake()
    {
        FaceLines = new Dictionary<Vector3, LineRenderer[]>();
        FaceLines.Add(Vector3.up, TopFaceLines);
        FaceLines.Add(Vector3.down, BottomFaceLines);
        FaceLines.Add(Vector3.left, LeftFaceLines);
        FaceLines.Add(Vector3.right, RightFaceLines);
        FaceLines.Add(Vector3.forward, FrontFaceLines);
        FaceLines.Add(Vector3.back, BackFaceLines);
    }

    void Update()
    {
        if(NumberText)
        {
            Vector3 CameraPosition = Camera.main.transform.position;
            NumberText.transform.LookAt(transform.position - (CameraPosition - transform.position), Camera.main.transform.up);
        }

        // Hide all lines
        foreach(LineRenderer Line in GetComponentsInChildren<LineRenderer>())
        {
            Line.gameObject.SetActive(false);
        } 

        // Only show the lines on the faces facing the camera
        Vector3 CameraForward = Camera.main.transform.forward;
        foreach(Vector3 FaceDirection in FaceLines.Keys)
        {
            bool FacingCamera = Vector3.Dot(transform.TransformDirection(FaceDirection), CameraForward) < 0.0f;
            if(FacingCamera)
            {
                foreach(LineRenderer Line in FaceLines[FaceDirection])
                {
                    Line.gameObject.SetActive(true);
                }
            }
        }

        NumberText.color = new Color(1, 1, 1, 
            Mathf.MoveTowards(NumberText.color.a, IsHighlighted ? 1.0f : 0.5f, Time.deltaTime * 4.0f));
        NumberText.transform.localScale = Vector3.one * 
            Mathf.MoveTowards(NumberText.transform.localScale.x, IsHighlighted ? 1.1f : 1.0f, Time.deltaTime * 0.6f);

        if(IsHighlighted)
        {
            IsHighlighted = false;
        }
    }

    public void SetNearbyBombCount(int NearbyBombCount)
    {
        BombIndicatorColourSet ColourSet = DangerColorSets[Mathf.Clamp(NearbyBombCount - 1, 0, DangerColorSets.Length - 1)];

        if(NumberText)
        {
            NumberText.fontSharedMaterial = ColourSet.NumberTextMaterial;
            NumberText.text = NearbyBombCount.ToString();
        }
        
        if(FillRenderer)
        {
            Material Mat = FillRenderer.material;
            Mat.color = ColourSet.FillColour;
            FillRenderer.material = Mat;
        }
        
        foreach(LineRenderer Line in GetComponentsInChildren<LineRenderer>())
        {
            Line.startColor = ColourSet.LineColour;
            Line.endColor = ColourSet.LineColour;
        }
    }

    private void SetFaceLinesActive(LineRenderer[] LineRenderers, bool Active)
    {
        foreach(LineRenderer Renderer in LineRenderers)
        {
            Renderer.gameObject.SetActive(Active);
        }
    }

    public void SetHighlighted()
    {
        IsHighlighted = true;
    }
}
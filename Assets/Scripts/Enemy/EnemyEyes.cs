using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEyes : MonoBehaviour
{
    public BoxFace Face { get; private set; }

    public void Init(BoxFace Face)
    {
        this.Face = Face;
    }
}

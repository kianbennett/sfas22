using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private Board Board;
    [SerializeField] private Transform Target;

    [Header("Movement")]
    [SerializeField] private float MoveSpeed;
    [SerializeField] private float MaxVelocityChange;
    [SerializeField] private float RotationSpeed;
    [SerializeField] private float MaxRotationChange;
    [SerializeField] private float PlayerKnockbackForce;
    [SerializeField] private float PlayerKnockbackDampSpeed;

    [Header("Legs")]
    [SerializeField] private Transform LegsParent;
    [SerializeField] private EnemyLeg LegPrefab;
    [SerializeField] private LegParams LegParams;

    [Header("Eyes")]
    [SerializeField] private EnemyEyes EyesPrefab;
    [SerializeField] private float EyesOffset;
    
    private Rigidbody Rigidbody;
    private EnemyEyes CurrentEyes;
    private Dictionary<BoxFace, EnemyLeg> SpawnedLegs;
    private Vector3 KnockbackVelocity;

    private float BoardHeightInit;

    public bool IsExploding { get; private set; }
    public bool IsDeactivated { get; private set; }

    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();

        Board.SetOwner(this);
        BoardHeightInit = Board.transform.localPosition.y;
        SpawnedLegs = new Dictionary<BoxFace, EnemyLeg>();
    }

    void Start()
    {
        if(MenuManager.Instance.Hud)
        {
            MenuManager.Instance.Hud.RegisterEnemy(this);
        }
    }

    private void Update()
    {
        UpdateVelocity();
        UpdateRotation();
        UpdateBoardHeight();
    }

    public void Generate(int BombAreaWidth, int BombAreaHeight, int BombCount)
    {
        if(BombAreaWidth == 0 || BombAreaHeight == 0 || BombCount == 0 || !Board) return;

        Board.RechargeBoxes(BombAreaWidth, BombAreaHeight, BombCount);
        SpawnEyes();
        SpawnLegs();
    }

    public void SpawnEyes()
    {
        if(CurrentEyes)
        {
            Destroy(CurrentEyes.gameObject);
            CurrentEyes = null;
        }

        if(!Board) return;

        Board.CalculateOutwardBoxFaces();
        List<BoxFace> ForwardFaces = Board.OutwardBoxFaces.Where(o => o.Direction == Vector3.forward).ToList();

        if(ForwardFaces.Count() > 0)
        {
            List<BoxFace> ZOrderedFaces = ForwardFaces.OrderByDescending(o => o.Owner.transform.localPosition.z).ToList();

            float HighestZ = ZOrderedFaces[0].Owner.transform.localPosition.z;

            List<BoxFace> HighestZFaces = ForwardFaces.Where(o => o.Owner.transform.localPosition.z == HighestZ).ToList();
            List<BoxFace> FacesOrderedByDistToMiddle = HighestZFaces.OrderBy(o => Mathf.Abs(o.Owner.transform.localPosition.x)).ToList();

            if(FacesOrderedByDistToMiddle.Count() > 0)
            {
                BoxFace Face = FacesOrderedByDistToMiddle[0];
                Vector3 EyesPosition = Face.Owner.transform.localPosition + Vector3.forward * (Box.BoxSize / 2.0f + EyesOffset);
                EnemyEyes EyeObject = Instantiate(EyesPrefab, EyesPosition, Quaternion.identity);
                EyeObject.Init(Face);
                EyeObject.transform.SetParent(Board.transform, false);
                CurrentEyes = EyeObject;
            }
        }
    }

    public void SpawnLegs()
    {
        if(!Board) return;

        foreach(EnemyLeg Leg in SpawnedLegs.Values)
        {
            if(Leg) Destroy(Leg.gameObject);
            
        }
        SpawnedLegs.Clear();

        int FaceCount = Board.OutwardBoxFaces.Count;
        int CurrentFace = Random.Range(0, FaceCount);

        for(int i = 0; i < FaceCount; i += 2)
        {
            BoxFace Face = Board.OutwardBoxFaces[i];

            if(Face.Owner == CurrentEyes.Face.Owner && Face.Direction == Vector3.forward)
            {
                continue;
            }

            Vector3 LegDirection = Face.Direction;
            Vector3 LegPosition = Face.Owner.transform.localPosition + LegDirection * (Box.BoxSize / 2.0f);
            EnemyLeg LegObject = Instantiate(LegPrefab, LegPosition, Quaternion.identity);
            LegObject.transform.right = -LegDirection;
            LegObject.transform.SetParent(LegsParent, false);
            LegObject.Setup(this, LegParams, (i % 4 == 0) ? 0 : 0.5f + Random.value * 0.5f); // Every other leg has an offset of 1
            SpawnedLegs.Add(Face, LegObject);
        }
    }

    private void UpdateVelocity()
    {
        Vector3 TargetVelocity = transform.forward * MoveSpeed + KnockbackVelocity;
        KnockbackVelocity = Vector3.MoveTowards(KnockbackVelocity, Vector3.zero, Time.deltaTime * PlayerKnockbackDampSpeed);

        // Apply a force that attempts to reach our target velocity
        Vector3 Velocity = Rigidbody.velocity;
        Vector3 VelocityChange = (TargetVelocity - Velocity);
        VelocityChange.x = Mathf.Clamp(VelocityChange.x, -MaxVelocityChange, MaxVelocityChange);
        VelocityChange.z = Mathf.Clamp(VelocityChange.z, -MaxVelocityChange, MaxVelocityChange);
        VelocityChange.y = 0;
        Rigidbody.AddForce(VelocityChange, ForceMode.VelocityChange);
    }

    // From https://gamedev.stackexchange.com/questions/182850/rotate-rigidbody-to-face-away-from-camera-with-addtorque
    private void UpdateRotation()
    {
        if(MoveSpeed == 0 || !Target)
        {
            return;
        }
            
        Vector3 DirectionToTarget = Target.position - transform.position;
        DirectionToTarget.y = 0;
        Quaternion TargetRotation = Quaternion.LookRotation(DirectionToTarget, Vector3.up);
        Quaternion RotationChange = TargetRotation * Quaternion.Inverse(Rigidbody.rotation);

        // Convert to an angle-axis representation, with angle in range -180...180
        RotationChange.ToAngleAxis(out float Angle, out Vector3 Axis);
        if (Angle > 180f) 
        {
            Angle -= 360f;
        }

        // If we're already facing the right way, just stop
        // This avoids problems with the infinite axes ToAngleAxis gives us in this case
        if (Mathf.Approximately(Angle, 0)) 
        {
            Rigidbody.angularVelocity = Vector3.zero;
            return;
        }

        // Enforce a cap here on the maximum rotation allowed in a single step, to prevent overly jerky movemen
        Angle = Mathf.Clamp(Angle, -MaxRotationChange, MaxRotationChange);

        // Compute an angular velocity that will bring us to the target orientation in a single time step
        Angle *= Mathf.Deg2Rad;
        Vector3 TargetAngularVelocity = Vector3.zero;
        if(Time.deltaTime > 0)
        {
            TargetAngularVelocity = Axis * Angle * RotationSpeed;
        }

        // Apply a torque to reach the target velocity
        Rigidbody.AddTorque(TargetAngularVelocity - Rigidbody.angularVelocity, ForceMode.VelocityChange);
    }

    private void UpdateBoardHeight()
    {
        if(IsDeactivated) return;

        float HeightOffset = 0;
        if(SpawnedLegs != null && SpawnedLegs.Count > 0)
        {
            float StepHeightAverage = 0;
            foreach(EnemyLeg Leg in SpawnedLegs.Values)
            {
                if(Leg)
                {
                    StepHeightAverage += Leg.GetCurrentStepHeight();
                }
            }
            StepHeightAverage /= SpawnedLegs.Count;
            HeightOffset += StepHeightAverage;
        }
        
        Board.transform.position = new Vector3(Board.transform.position.x, BoardHeightInit + HeightOffset, Board.transform.position.z);
    }

    public Vector3 GetVelocity()
    {
        return Rigidbody.velocity;
    }

    public Vector3 GetAngularVelocity()
    {
        return Rigidbody.angularVelocity;
    }

    public void Knockback(Vector3 Direction)
    {
        Direction.Normalize();
        KnockbackVelocity = Direction * PlayerKnockbackForce;
    }

    public void SetTarget(Transform Target)
    {
        this.Target = Target;
    }

    public void OnBoxDestroy(Box Box, bool CameraShake)
    {
        List<BoxFace> FacesToRemove = new List<BoxFace>();
        foreach(BoxFace Face in SpawnedLegs.Keys)
        {
            if(Face.Owner == Box)
            {
                Destroy(SpawnedLegs[Face].gameObject);
                FacesToRemove.Add(Face);
            }
        }

        foreach(BoxFace Face in FacesToRemove)
        {
            SpawnedLegs.Remove(Face);
        }
        FacesToRemove.Clear();

        if(Box.IsDangerous)
        {
            if(CameraShake) CameraManager.Instance.ShakeCamera(0.8f, 0.01f, 0.2f);
            if(!IsExploding)
            {
                DestroyEyesOnBox(Box);
                Explode(Box, false);
            }
        }
        else
        {
            if(CameraShake) CameraManager.Instance.ShakeCamera(0.1f, 0.01f, 0.05f);
            if(CurrentEyes && CurrentEyes.Face.Owner == Box)
            {
                SpawnEyes();
            }
        }
    }

    public void Explode(Box ExplodingBox, bool Smoke)
    {
        if(!IsExploding)
        {
            IsExploding = true;
            StartCoroutine(ExplodeIEnum(ExplodingBox, Smoke));
        }
    }

    public void Deactivate()
    {
        if(!IsDeactivated)
        {
            StartCoroutine(DeactivateIEnum());
            IsDeactivated = true;
            GameService.Instance.IncreaseScore(10 * Board.BombCount);
        }
    }

    private IEnumerator ExplodeIEnum(Box ExplodingBox, bool Smoke)
    {
        Board.ClearBombIndicators();

        Box[] BoxArray = Board.GetAllBoxes();

        if(ExplodingBox)
        {
            BoxArray = BoxArray.OrderBy(o => Vector3.Distance(o.transform.position, ExplodingBox.transform.position)).ToArray();
        }

        foreach(Box Box in BoxArray)
        {
            if(!Box) continue;
            DestroyEyesOnBox(Box);
            Board.RemoveBoxFromGrid(Box);
            Box.Explode(Smoke);
            OnBoxDestroy(Box, !Smoke);
            yield return new WaitForSeconds(0.04f);
        }

        Destroy(gameObject);

        if(!Smoke)
        {
            GameService.Instance.Player.Damage(
                GameService.Instance.Player.transform.position - transform.position, 20);
        }
    }

    private IEnumerator DeactivateIEnum()
    {
        if(CurrentEyes)
        {
            Destroy(CurrentEyes.gameObject);
        }
        foreach(EnemyLeg Leg in SpawnedLegs.Values)
        {
            Destroy(Leg.gameObject);
        }
        SpawnedLegs.Clear();

        Board.ClearBombIndicators();

        Rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        Rigidbody.isKinematic = true;

        while(Board.transform.localPosition.y > 0.5f)
        {
            Board.transform.localPosition = Vector3.MoveTowards(Board.transform.localPosition, Vector3.up * 0.5f, Time.deltaTime * 5f);
            yield return null;
        }

        yield return new WaitForSeconds(2.0f);

        Explode(null, true);
    }

    private void DestroyEyesOnBox(Box Box)
    {
        if(CurrentEyes && CurrentEyes.Face.Owner == Box)
        {
            Destroy(CurrentEyes.gameObject);
            CurrentEyes = null;
        }
    }

    public void SetSpeed(float MoveSpeed, float RotationSpeed)
    {
        this.MoveSpeed = MoveSpeed;
        this.RotationSpeed = RotationSpeed;
    }

    void OnCollisionEnter(Collision other) 
    {
        if(other.gameObject.GetComponent<Enemy>())
        {
            Knockback(transform.position - other.gameObject.transform.position);
        }
    }
}
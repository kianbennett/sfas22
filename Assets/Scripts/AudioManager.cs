﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager> 
{
    [System.Serializable]
    public class CustomAudio 
    {
        [SerializeField] private AudioClip Clip;
        [SerializeField] private float _volume = 1;

        public float Volume { get { return _volume; } }

        public void PlayAsMusic() 
        {
            Instance.SourceMusic.clip = Clip;
            Instance.SourceMusic.Play();
            Instance.SourceMusic.volume = (OptionsManager.Instance.VolumeMusic.Value / 10f) * _volume;
            Instance.MusicPlaying = this;
        }

        public void PlayAsSFX(float pitch = 1) 
        {
            Instance.SourceSFX.pitch = pitch;
            Instance.SourceSFX.PlayOneShot(Clip, _volume);
        }
    }

    public AudioSource SourceMusic;
    public AudioSource SourceSFX;

    [Header("Music")]
    public CustomAudio MusicMenu;
    public CustomAudio MusicLevel;

    [Header("SFX")]
    public CustomAudio SfxButtonClick;
    public CustomAudio SfxBlip;
    public CustomAudio SfxGameOver;
    public CustomAudio SfxPlayerDeath;
    public CustomAudio SfxBullet;
    public CustomAudio SfxExplosion;
    public CustomAudio SfxNoEnergy;
    public CustomAudio SfxDefuse;
    public CustomAudio SfxSwitchAimType;
    public CustomAudio SfxDash;
    public CustomAudio SfxPlayerHit;

    private bool MusicMuted;
    private CustomAudio MusicPlaying;

    public float MusicPlayingVolume { get { return MusicPlaying != null ? MusicPlaying.Volume : 0; } }

    void Update() 
    {
        if(SourceMusic.isPlaying) 
        {
            float GlobalVolume = OptionsManager.Instance.VolumeMusic.Value / 10f;
            float MusicVolume = MusicMuted ? 0 : GlobalVolume * MusicPlayingVolume;
            SourceMusic.volume = Mathf.MoveTowards(SourceMusic.volume, MusicVolume, Time.deltaTime * 2f);
        }
    }

    public void PlayButtonClick() 
    {
        SfxButtonClick.PlayAsSFX();
    }

    public void FadeOutMusic() 
    {
        MusicMuted = true;
    }

    public void FadeInMusic() 
    {
        MusicMuted = false;
        SourceMusic.volume = 0;
    }

    public void PauseMusic() 
    {
        SourceMusic.Pause();
    }

    public void ResumeMusic() 
    {
        SourceMusic.Play();
    }
}

using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject MainMenuPanel;
    [SerializeField] private OptionsPanel OptionsPanel;
    [SerializeField] private HighscoresPanel HighscoresPanel;
    [SerializeField] private GameObject ControlsPanel;
    [SerializeField] private Enemy Enemy;
    [SerializeField] private Transform EnemyTarget;
    [SerializeField] private Transform Floor1, Floor2;

    void Start()
    {
        Enemy.Generate(4, 4, 6);
        AudioManager.Instance.MusicMenu.PlayAsMusic();
    }

    void Update()
    {
        EnemyTarget.position = Enemy.transform.position + Enemy.transform.forward * 10;
        // If the enemy passes the second floor, put the first floor in front of the second and swap floor numbers
        if(Enemy.transform.position.x < Floor2.transform.position.x)
        {
            Floor1.transform.position = new Vector3(Floor2.transform.position.x - 150, Floor1.transform.position.y, Floor1.transform.position.z);
            Transform Intermediate = Floor1;
            Floor1 = Floor2;
            Floor2 = Intermediate;
        }
    }

    public void Play()
    {
        SceneManager.LoadScene("Level");
        AudioManager.Instance.PlayButtonClick();
    }

    public void Options()
    {
        ShowPanel(OptionsPanel.gameObject);
        OptionsPanel.Show();
    }

    public void CloseOptions()
    {
        HidePanel(OptionsPanel.gameObject);
    }

    public void Highscores()
    {
        ShowPanel(HighscoresPanel.gameObject);
        HighscoresPanel.Show(null);
    }

    public void CloseHighscores()
    {
        HidePanel(HighscoresPanel.gameObject);
        HighscoresPanel.Hide();
    }

    public void Controls()
    {
        ShowPanel(ControlsPanel);
    }

    public void HideControls()
    {
        HidePanel(ControlsPanel);
    }

    private void ShowPanel(GameObject Panel)
    {
        MainMenuPanel.SetActive(false);   
        Panel.SetActive(true);
        AudioManager.Instance.PlayButtonClick();
        CameraManager.Instance.SetPostProcessingEffectEnabled<DepthOfField>(true);
    }

    private void HidePanel(GameObject Panel)
    {
        MainMenuPanel.SetActive(true);
        Panel.SetActive(false);
        AudioManager.Instance.PlayButtonClick();
        CameraManager.Instance.SetPostProcessingEffectEnabled<DepthOfField>(false);
    }

    public void Quit()
    {
        AudioManager.Instance.PlayButtonClick();
        Application.Quit();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuControls : Menu
{
    public void Return()
    {
        MenuManager.Instance.ShowMenu(MenuManager.Instance.MenuPause);
    }
}

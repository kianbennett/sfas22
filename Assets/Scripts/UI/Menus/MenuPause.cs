using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuPause : Menu
{
    [SerializeField] private TextMeshProUGUI TextScore;

    public override void Show()
    {
        base.Show();

        TextScore.text = "score\n<size=26>" + MenuManager.ScoreToString(GameService.Instance.Score) + "</size>";
    }

    public void Continue()
    {
        GameService.Instance.SetPaused(false);
    }

    public void Options()
    {
        MenuManager.Instance.ShowMenu(MenuManager.Instance.MenuOptions);
    }

    public void Controls()
    {
        MenuManager.Instance.ShowMenu(MenuManager.Instance.MenuControls);
    }

    public void Quit()
    {
        AudioManager.Instance.PlayButtonClick();
        SceneManager.LoadScene("MainMenu");
    }
}

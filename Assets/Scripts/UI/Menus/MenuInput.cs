using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuInput : Menu 
{
    [SerializeField] private TMP_InputField InputField;
    [SerializeField] private Button ButtonConfirm;

    void Update() 
    {
        ButtonConfirm.interactable = !string.IsNullOrWhiteSpace(InputField.text);;
    }

    public override void Show()
    {
        base.Show();

        InputField.text = "";
        InputField.Select();
        ButtonConfirm.interactable = false;
        ButtonConfirm.GetComponentInChildren<ButtonText>().UpdateTextColour();
    }

    public void Confirm()
    {
        Highscore Highscore = new Highscore(InputField.text, GameService.Instance.Score);
        MenuManager.Instance.MenuHighScores.SetHighscoreToUpload(Highscore);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.MenuHighScores);
    }

    public void Return()
    {
        MenuManager.Instance.MenuHighScores.SetHighscoreToUpload(null);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.MenuHighScores);
    }
}
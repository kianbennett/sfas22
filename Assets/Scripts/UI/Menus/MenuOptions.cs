using UnityEngine;

public class MenuOptions : Menu 
{
    [SerializeField] private OptionsPanel OptionsPanel;

    public override void Show()
    {
        base.Show();

        OptionsPanel.Show();
    }

    public void Return()
    {
        MenuManager.Instance.ShowMenu(MenuManager.Instance.MenuPause);
    }
}
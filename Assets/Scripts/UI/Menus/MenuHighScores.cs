using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuHighScores : Menu 
{
    [SerializeField] private Button ButtonUpload;
    [SerializeField] private TextMeshProUGUI TextScore;
    [SerializeField] private HighscoresPanel HighscoresPanel;

    private Highscore? HighscoreToUpload;
    private bool HasUploaded;

    void Update()
    {
        ButtonUpload.interactable = HighscoresPanel.HasRetrievedScores && !HasUploaded;
    }

    public override void Show()
    {
        base.Show();
        ButtonUpload.interactable = false;
        ButtonUpload.GetComponentInChildren<ButtonText>().UpdateTextColour();
        HighscoresPanel.Show(HighscoreToUpload);
        TextScore.text = "score\n<size=26>" + MenuManager.ScoreToString(GameService.Instance.Score) + "</size>";
    }

    public override void Hide()
    {
        base.Hide();
        HighscoresPanel.Hide();
    }

    public void SetHighscoreToUpload(Highscore? Highscore)
    {
        HighscoreToUpload = Highscore;
        if(Highscore != null)
        {
            HasUploaded = true;
        }
    }

    public void Upload()
    {
        MenuManager.Instance.ShowMenu(MenuManager.Instance.MenuInput);
    }

    public void Return()
    {
        MenuManager.Instance.ShowMenu(MenuManager.Instance.MenuGameOver);
    }
}
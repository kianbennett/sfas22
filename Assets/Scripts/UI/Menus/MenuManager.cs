using UnityEngine;
using UnityEngine.Rendering.Universal;

public class MenuManager : Singleton<MenuManager>
{
    public HUD Hud;
    public MenuControls MenuControls;
    public MenuGameOver MenuGameOver;
    public MenuHighScores MenuHighScores;
    public MenuInput MenuInput;
    public MenuOptions MenuOptions;
    public MenuPause MenuPause;

    private Menu CurrentMenu;

    public void ShowMenu(Menu Menu)
    {
        if(CurrentMenu) CurrentMenu.Hide();
        if(Menu) Menu.Show();
        
        CurrentMenu = Menu;

        if(Hud)
        {
            Hud.gameObject.SetActive(Menu == null);
        }

        CameraManager.Instance.SetPostProcessingEffectEnabled<DepthOfField>(Menu != null);
        AudioManager.Instance.PlayButtonClick();
    }

    public static string ScoreToString(int Score)
    {
        string ScoreString = Score.ToString("D8");
        int FirstNonZeroPosition = 0;

        for(int i = 0; i < ScoreString.Length; i++)
        {
            if(ScoreString[i] != '0')
            {
                FirstNonZeroPosition = i;
                break;
            }
        }

        if(FirstNonZeroPosition == 0) FirstNonZeroPosition = ScoreString.Length - 1;

        string Zeros = ScoreString.Substring(0, FirstNonZeroPosition);
        string Remaining = ScoreString.Substring(FirstNonZeroPosition, ScoreString.Length - FirstNonZeroPosition);

        return "<alpha=#88>" + Zeros + "</color>" + Remaining;
    }
}

using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuGameOver : Menu 
{
    [SerializeField] private TextMeshProUGUI TextScore;

    public override void Show()
    {
        base.Show();

        TextScore.text = "score\n<size=26>" + MenuManager.ScoreToString(GameService.Instance.Score) + "</size>";
    }

    public void PlayAgain()
    {
        // Reload the current scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Highscores()
    {
        MenuManager.Instance.MenuHighScores.SetHighscoreToUpload(null);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.MenuHighScores);
    }

    public void Quit()
    {
        AudioManager.Instance.PlayButtonClick();
        SceneManager.LoadScene("MainMenu");
    }
}
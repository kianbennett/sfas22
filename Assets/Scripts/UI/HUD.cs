using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HUD : MonoBehaviour
{
    public StatBar HealthBar;
    public StatBar EnergyBar;

    [SerializeField] private Animator EnergyBarAnimator;
    [SerializeField] private TextMeshProUGUI TextScore;
    [SerializeField] private Color EnergyBarFillColourDestroy;
    [SerializeField] private Color EnergyBarOutlineColourDestroy;
    [SerializeField] private Color EnergyBarFillColourDefuse;
    [SerializeField] private Color EnergyBarOutlineColourDefuse;

    [Header("Enemy Arrows")]
    [SerializeField] private Transform EnemyArrowsContainer;
    [SerializeField] private GameObject EnemyArrowPrefab;

    private Dictionary<GameObject, Enemy> EnemyArrows;
    private List<GameObject> ArrowsToRemove;

    void Awake()
    {
        EnemyArrows = new Dictionary<GameObject, Enemy>();
        ArrowsToRemove = new List<GameObject>();
        UpdateScore(0);
    }

    void Update()
    {
        foreach(GameObject Arrow in EnemyArrows.Keys)
        {
            UpdateArrowPosition(Arrow);
        }
        foreach(GameObject Arrow in ArrowsToRemove) 
        {
            EnemyArrows.Remove(Arrow);
        }
        ArrowsToRemove.Clear();
    }

    public void UpdateScore(int Score)
    {
        TextScore.text = MenuManager.ScoreToString(Score);
    }

    public void ShowEnergyBar()
    {
        EnergyBarAnimator.SetTrigger("Appear");
    }

    public void HideEnergyBar()
    {
        EnergyBarAnimator.SetTrigger("Hide");
    }

    public void SetEnergyBarColour(AimType AimType)
    {
        if(!MenuManager.Instance.Hud) return;

        if(AimType == AimType.Destroy)
        {
            MenuManager.Instance.Hud.EnergyBar.SetBarColours(
                EnergyBarFillColourDestroy, EnergyBarOutlineColourDestroy
            );
        }
        else if(AimType == AimType.Defuse)
        {
            MenuManager.Instance.Hud.EnergyBar.SetBarColours(
                EnergyBarFillColourDefuse, EnergyBarOutlineColourDefuse
            );
        }
    }

    public void RegisterEnemy(Enemy Enemy)
    {
        if(EnemyArrows.ContainsValue(Enemy)) return;

        GameObject Arrow = Instantiate(EnemyArrowPrefab, Vector3.zero, Quaternion.identity, EnemyArrowsContainer);
        EnemyArrows.Add(Arrow, Enemy);
    }

    private void UpdateArrowPosition(GameObject Arrow)
    {
        if(!EnemyArrows.ContainsKey(Arrow))
        {
            Destroy(Arrow);
            return;
        }

        Enemy Enemy = EnemyArrows[Arrow];
        if(Enemy == null)
        {
            ArrowsToRemove.Add(Arrow);
            return;
        }

        Vector3 EnemyScreenPos = CameraManager.Instance.MainCamera.WorldToScreenPoint(Enemy.transform.position);
        EnemyScreenPos.z = 0;

        bool IsOffScreen = EnemyScreenPos.x < 0 || EnemyScreenPos.x > Screen.width ||
            EnemyScreenPos.y < 0 || EnemyScreenPos.y > Screen.height;
            
        Arrow.SetActive(IsOffScreen);

        if(IsOffScreen)
        {
            float Padding = 20.0f;
            Arrow.transform.position = new Vector2(
                Mathf.Clamp(EnemyScreenPos.x, Padding, Screen.width - Padding),
                Mathf.Clamp(EnemyScreenPos.y, Padding, Screen.height - Padding)
            );

            float Angle = Vector3.SignedAngle(EnemyScreenPos - new Vector3(Screen.width / 2, Screen.height / 2), Vector3.up, Vector3.forward);
            Arrow.transform.rotation = Quaternion.Euler(Vector3.forward * -Angle);
        }
    }
}

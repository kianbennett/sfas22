using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatBar : MonoBehaviour
{
    [SerializeField] private RectTransform BarContainerTransform;
    [SerializeField] private RectTransform BarTransform;
    [SerializeField] private RectTransform SecondaryBarTransform;
    [SerializeField] private Image BarFillImage, BarOutlineImage;
    [SerializeField] private Color BarShakeColour;

    private float BarWidth;
    private float BarPercentage;
    private float SecondaryBarPercentage;
    private Color BarFillColourInit, BarOutlineColourInit;

    private Coroutine ShakeCoroutine;

    void Awake() 
    {
        BarFillColourInit = BarFillImage.color;
        BarOutlineColourInit = BarOutlineImage.color;
        BarWidth = GetComponent<RectTransform>().sizeDelta.x;
        BarPercentage = SecondaryBarPercentage = 1;
        BarTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, BarPercentage);
        if(SecondaryBarTransform)
        {
            SecondaryBarTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, BarPercentage);
        }
    }

    void Update()
    {
        BarTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, BarPercentage * BarWidth);

        if(SecondaryBarTransform)
        {
            if(BarPercentage > SecondaryBarPercentage)
            {
                SecondaryBarPercentage = BarPercentage;
            }
            else
            {
                SecondaryBarPercentage = Mathf.Lerp(SecondaryBarPercentage, BarPercentage, Time.deltaTime * 5);
            }
            SecondaryBarTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, SecondaryBarPercentage * BarWidth);
        }
    }

    public void SetValues(float StatMax, float StatCurrent)
    {
        if(StatMax == 0) return;

        BarPercentage = StatCurrent / StatMax;
    }

    public void Shake(float Distance, float Interval, float Duration)
    {
        if(ShakeCoroutine != null) StopCoroutine(ShakeCoroutine);
        StartCoroutine(ShakeIEnum(Distance, Interval, Duration, BarShakeColour));
    }

    private IEnumerator ShakeIEnum(float Distance, float Interval, float Duration, Color BarColour)
    {
        BarFillImage.color = BarColour;
        float TimeRemaining = Duration;
        while(TimeRemaining > 0)
        {
            TimeRemaining -= Interval;
            float OffsetX = Random.Range(-Distance, Distance);
            float OffsetY = Random.Range(-Distance, Distance);
            BarContainerTransform.anchoredPosition = new Vector2(OffsetX, OffsetY);
            yield return new WaitForSeconds(Interval);
        }
        BarContainerTransform.anchoredPosition = Vector2.zero;
        BarFillImage.color = BarFillColourInit;
    }

    public void SetBarColours(Color FillColour, Color OutlineColour)
    {
        BarFillImage.color = BarFillColourInit = FillColour;
        BarOutlineImage.color = BarOutlineColourInit = OutlineColour;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsPanel : MonoBehaviour
{
    [SerializeField] private Slider MusicVolumeSlider, SfxVolumeSlider;
    [SerializeField] private Toggle HighQualityToggle, FullscreenToggle;

    private bool PlaySfx;

    public void Show() 
    {
        // Set PlaySfx to false before setting values so sound effect doesn't play when the menu is opened (callbacks will be called here)
        PlaySfx = false;

        MusicVolumeSlider.value = OptionsManager.Instance.VolumeMusic.Value;
        SfxVolumeSlider.value = OptionsManager.Instance.VolumeSFX.Value;
        HighQualityToggle.isOn = OptionsManager.Instance.HighQuality.BoolValue;
        FullscreenToggle.isOn = OptionsManager.Instance.Fullscreen.BoolValue;

        PlaySfx = true;
    }

    // UI Callbacks

    public void SetMusicVolumeValue() 
    {
        OptionsManager.Instance.VolumeMusic.SetValue((int) MusicVolumeSlider.value, true);
        if(PlaySfx) AudioManager.Instance.SfxBlip.PlayAsSFX();
    }

    public void SetSFXVolumeValue() 
    {
        OptionsManager.Instance.VolumeSFX.SetValue((int) SfxVolumeSlider.value, true);
        if(PlaySfx) AudioManager.Instance.SfxBlip.PlayAsSFX();
    }

    public void SetHighQualityValue() 
    {
        OptionsManager.Instance.HighQuality.SetValue(HighQualityToggle.isOn ? 1 : 0, true);
        if(PlaySfx) AudioManager.Instance.PlayButtonClick();
    }

    public void SetFullscreenValue() 
    {
        OptionsManager.Instance.Fullscreen.SetValue(FullscreenToggle.isOn ? 1 : 0, true);
        if(PlaySfx) AudioManager.Instance.PlayButtonClick();
    }
}

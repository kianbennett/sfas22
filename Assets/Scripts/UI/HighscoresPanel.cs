using System.Collections;
using UnityEngine;
using TMPro;

public struct Highscore 
{
    public string Name;
    public int Score;

    public Highscore(string Name, int Score)
    {
        this.Name = Name;
        this.Score = Score;
    }
}

public class HighscoresPanel : MonoBehaviour
{
    [SerializeField] private GameObject ScoresContainer;
    [SerializeField] private TextMeshProUGUI[] TextNames;
    [SerializeField] private TextMeshProUGUI[] TextScores;
    [SerializeField] private RectTransform LoadingIcon;
    [SerializeField] private float LoadingIconSpinSpeed;
    [SerializeField] private TextMeshProUGUI TextError;

    private Coroutine WebRequestCoroutine;

    public bool HasRetrievedScores { get; private set; }

    void Update()
    {
        if(LoadingIcon && LoadingIcon.gameObject.activeInHierarchy)
        {
            LoadingIcon.Rotate(Vector3.forward * LoadingIconSpinSpeed * Time.deltaTime);
        }
    }

    public void Show(Highscore? HighscoreToUpload)
    {
        gameObject.SetActive(true);
        if(HighscoreToUpload != null)
        {
            StartRequest(WebRequestUtil.UploadHighScoreIEnum(HighscoreToUpload.Value, OnRequestFinish, OnRequestError));    
        }
        else
        {
            StartRequest(WebRequestUtil.GetHighScoresIEnum(TextNames.Length, OnRequestFinish, OnRequestError));
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        StopRequest();
    }

    private void StartRequest(IEnumerator IEnum) 
    {
        if (WebRequestCoroutine != null) StopCoroutine(WebRequestCoroutine);
        WebRequestCoroutine = StartCoroutine(IEnum);

        ScoresContainer.SetActive(false);
        LoadingIcon.gameObject.SetActive(true);
        TextError.gameObject.SetActive(false);

        HasRetrievedScores = false;
    }

    private void StopRequest() 
    {
        if(WebRequestCoroutine != null) StopCoroutine(WebRequestCoroutine);
        HasRetrievedScores = false;
    }

    public void OnRequestFinish(string Results)
    {
        LoadingIcon.gameObject.SetActive(false);
        ScoresContainer.SetActive(true);
        Highscore[] Highscores = WebRequestUtil.GetHighscoresFromString(Results);
        for(int i = 0; i < TextNames.Length; i++)
        {
            if(i <= Highscores.Length - 1) 
            {
                TextNames[i].text = "<alpha=#88>" + (i + 1) + ". </color>" + Highscores[i].Name;
                TextScores[i].text = MenuManager.ScoreToString(Highscores[i].Score);
            }
            else
            {
                TextNames[i].text = "<alpha=#88>" + (i + 1) + ".</color>";
                TextScores[i].text = "<alpha=#88>-</color>";
            }
            
        }

        HasRetrievedScores = true;
    }

    public void OnRequestError()
    {
        LoadingIcon.gameObject.SetActive(false);
        TextError.gameObject.SetActive(true);
        ScoresContainer.SetActive(false);
    }
}
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ButtonText : MonoBehaviour 
{
    [SerializeField] private TextMeshProUGUI Text;
    [SerializeField] private Button Button;
    [SerializeField] private float AlphaWhenDisabled;

    void Update()
    {
        UpdateTextColour();
    }

    public void UpdateTextColour()
    {
        Color TextColor = Text.color;
        TextColor.a = Button.interactable ? 1 : AlphaWhenDisabled;
        Text.color = TextColor;
    }
}
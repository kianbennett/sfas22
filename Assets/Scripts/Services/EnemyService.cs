using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyService : MonoBehaviour
{
    [Header("Enemy Spawning")]
    [SerializeField] private bool SpawnEnemies;
    [SerializeField] private Enemy EnemyPrefab;
    [SerializeField] private float SpawnInterval;
    // [SerializeField] private int MaxEnemyCount;
    [SerializeField] private int BombAreaSizeMin, BombAreaSizeMax;

    [Header("Bomb Indicators")]
    [SerializeField] private float BombIndicatorOutlineScrollSpeed;
    [SerializeField] private Material BombIndicatorOutlineMaterial;

    private List<Enemy> SpawnedEnemies;
    private Coroutine SpawnEnemiesCoroutine;
    private int MaxEnemyCount = 1;

    void Awake()
    {
        SpawnedEnemies = new List<Enemy>();
        if(SpawnEnemies)
        {
            SpawnEnemiesCoroutine = StartCoroutine(SpawnEnemiesIEnum());
        }
    }

    void Update()
    {
        if(BombIndicatorOutlineMaterial)
        {
            Vector2 BombIndicatorOutlineTextureOffset = new Vector2(Time.time * BombIndicatorOutlineScrollSpeed, 0);
            BombIndicatorOutlineMaterial.mainTextureOffset = BombIndicatorOutlineTextureOffset;
        }

        for(int i = SpawnedEnemies.Count - 1; i >= 0; --i)
        {
            if(!SpawnedEnemies[i]) 
            {
                SpawnedEnemies.RemoveAt(i);
            }
        }

        double TimeElapsed = GameService.Instance.ElapsedTime;
        if(TimeElapsed > 15)
        {
            MaxEnemyCount = 2;
        }
        if(TimeElapsed > 60)
        {
            MaxEnemyCount = 3;
        }
        if(TimeElapsed > 240)
        {
            MaxEnemyCount = 4;
        }
    }

    void OnDestroy()
    {
        if(BombIndicatorOutlineMaterial)
        {
            BombIndicatorOutlineMaterial.mainTextureOffset = Vector2.zero;
        }
    }

    public void StopSpawningEnemies()
    {
        if(SpawnEnemiesCoroutine != null)
        {
            StopCoroutine(SpawnEnemiesCoroutine);
        }
    }

    private IEnumerator SpawnEnemiesIEnum()
    {
        SpawnEnemyRandomlyNearPlayer(false);
        while(true)
        {
            if(SpawnedEnemies.Count < MaxEnemyCount)
            {
                yield return new WaitForSeconds(SpawnInterval);
                SpawnEnemyRandomlyNearPlayer(SpawnedEnemies.Count >= 2);
            }

            yield return null;
        }
    }

    private void SpawnEnemy(Vector3 Position, Quaternion Rotation, bool IsMini)
    {
        int BombAreaWidth = Random.Range(BombAreaSizeMin, BombAreaSizeMax);
        int BombAreaHeight = Random.Range(BombAreaSizeMin, BombAreaSizeMax);
        int BombCount = (int) Mathf.Sqrt(BombAreaWidth * BombAreaHeight) * 2;

        Enemy SpawnedEnemy = Instantiate(EnemyPrefab, Position, Rotation);
        if(IsMini)
        {
            BombAreaWidth = 2;
            BombAreaHeight = 3;
            BombCount = 3;
            SpawnedEnemy.SetSpeed(2.8f, 10);
        }
        SpawnedEnemy.Generate(BombAreaWidth, BombAreaHeight, BombCount);
        SpawnedEnemy.SetTarget(GameService.Instance.Player.transform);
        SpawnedEnemies.Add(SpawnedEnemy);
    }

    private void SpawnEnemyRandomlyNearPlayer(bool IsMini)
    {
        if(!GameService.Instance.Player) return;
        
        Vector3 PositionOffset = Quaternion.Euler(Vector3.up * Random.Range(0f, 360f)) * Vector3.forward * Random.Range(5f, 12f);
        Vector3 Position = GameService.Instance.Player.transform.position + PositionOffset;
        SpawnEnemy(Position, Quaternion.LookRotation(GameService.Instance.Player.transform.position - Position, Vector3.up), IsMini);
    }
}

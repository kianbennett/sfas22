using UnityEngine;

public class GameService : Singleton<GameService> 
{
    public Player Player;
    
    public double ElapsedTime { get; private set; }
    public int Score { get; private set; }
    public bool IsPaused { get; private set; }
    public bool IsGameOver { get; private set; }

    protected override void Awake()
    {
        AudioManager.Instance.MusicLevel.PlayAsMusic();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SetPaused(!IsPaused);
        }

        ElapsedTime += Time.deltaTime;
    }

    public void IncreaseScore(int Value)
    {
        if(IsGameOver) return;
        Score += Value;
        MenuManager.Instance.Hud.UpdateScore(Score);
    }

    public void SetPaused(bool Paused)
    {
        if(IsGameOver) return;
        
        IsPaused = Paused;
        Time.timeScale = Paused ? 0.0f : 1.0f;

        if(Paused)
        {
            MenuManager.Instance.ShowMenu(MenuManager.Instance.MenuPause);
        }
        else
        {
            MenuManager.Instance.ShowMenu(null);
        }
    }

    public void EndGame()
    {
        Destroy(Player.gameObject);
        IsGameOver = true;
        MenuManager.Instance.ShowMenu(MenuManager.Instance.MenuGameOver);
        AudioManager.Instance.SfxGameOver.PlayAsSFX(0.5f);
        AudioManager.Instance.SfxPlayerDeath.PlayAsSFX(0.5f);
    }
}
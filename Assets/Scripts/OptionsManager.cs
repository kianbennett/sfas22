﻿using UnityEngine;
using UnityEngine.Rendering.Universal;
using System;

public class OptionsManager : Singleton<OptionsManager> 
{
    public class Option 
    {
        private string Key;
        private int DefaultValue;
        private int _value;

        private Action<int> OnValueChange;

        public int Value { get { return _value; } }
        public bool BoolValue { get { return _value > 0; } }

        public Option(string Key, int DefaultValue, Action<int> OnValueChange) 
        {
            this.Key = Key;
            _value = this.DefaultValue = DefaultValue;
            this.OnValueChange = OnValueChange;
            LoadFromPlayerPrefs();
        }

        public void SetValue(int Value, bool Save) 
        {
            this._value = Value;
            OnValueChange(Value);
            if (Save) SaveToPlayerPrefs();
        }

        public void ResetToDefault() 
        {
            SetValue(DefaultValue, true);
        }

        public void SaveToPlayerPrefs() 
        {
            PlayerPrefs.SetInt(Key, _value);
            PlayerPrefs.Save();
        }

        public void LoadFromPlayerPrefs() 
        {
            if(PlayerPrefs.HasKey(Key)) 
            {
                SetValue(PlayerPrefs.GetInt(Key), false);
            } 
            else 
            {
                SetValue(DefaultValue, false);
            }
        }
    }

    public Option VolumeMusic, VolumeSFX, HighQuality, Fullscreen;

    // Default windowed resolution
    private Vector2Int ScreenSizeInit;

    protected override void Awake() 
    {
        base.Awake();
        ScreenSizeInit = new Vector2Int(Screen.width, Screen.height);
        
        VolumeMusic = new Option("VolumeMusic", 8, OnChangeVolumeMusic);
        VolumeSFX = new Option("VolumeSFX", 8, OnChangeVolumeSFX);
        HighQuality = new Option("HighQuality", 1, OnChangeHighQuality);
        Fullscreen = new Option("Fullscreen", 1, OnChangeFullscreen);
    }

    private void OnChangeVolumeMusic(int value) 
    {
        AudioManager.Instance.SourceMusic.volume = value / 10f * AudioManager.Instance.MusicPlayingVolume;
    }

    private void OnChangeVolumeSFX(int value) 
    {
        AudioManager.Instance.SourceSFX.volume = value / 10f;
    }

    private void OnChangeHighQuality(int value) 
    {
        if(!CameraManager.Instance) return;

        CameraManager.Instance.SetPostProcessingEffectEnabled<Bloom>(value == 1);
        CameraManager.Instance.SetAntialiasingEnabled(value == 1);
    }

    private void OnChangeFullscreen(int Value) 
    {
        bool Fullscreen = Value > 0;
        if(Fullscreen) 
        {
            Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
        } 
        else 
        {
            Screen.SetResolution(1280, 720, false);
        }
    }
}
